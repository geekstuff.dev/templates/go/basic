#!make

# NOTE: This Makefile is made with local dev usage in mind

# hide entering/leaving directory messages
MAKEFLAGS += --no-print-directory

CI_PROJECT_URL ?= https://gitlab.com/geekstuff.dev/templates/go/basic
CI_REGISTRY_IMAGE ?= tpl/go_kit
GO_SOURCE ?= golang
GO_VERSION ?= 1.18
GO_ALPINE_TAG ?= ${GO_VERSION}-alpine
GO_DEBIAN_TAG ?= ${GO_VERSION}-bullseye
PROJECT_TAG ?= v0.0.0

TEMPLATE_ALPINE_PKG ?= go_kit_alpine_devcontainer_${PROJECT_TAG}
TEMPLATE_DEBIAN_PKG ?= go_kit_debian_devcontainer_${PROJECT_TAG}

PACKAGE_DIR ?= packages

all: info

.PHONY: info
info:
	@echo "Welcome to ${CI_PROJECT_URL}"
	@echo ""
	@echo "Useful commands:"
	@echo "  make alpine.docker.devcontainer"
	@echo "  make debian.docker.devcontainer"
	@echo "  make alpine.docker.gitlab-ci"
	@echo "  make debian.docker.gitlab-ci"

# IMPORTANT: This job is used in CI on top of dev
.PHONY: .docker
.docker:
	docker build \
		-t ${CI_REGISTRY_IMAGE}/${PROJECT_TAG}/${SPECIFIC_IMAGE}:${GO_TAG} \
		-f docker-images/Dockerfile \
		--target ${SPECIFIC_IMAGE} \
		--cache-from ${CI_REGISTRY_IMAGE}/${PROJECT_TAG}/${SPECIFIC_IMAGE}:${GO_TAG} \
		--cache-from ${CI_REGISTRY_IMAGE}/${PROJECT_TAG}/main:${GO_TAG} \
		--build-arg CI_PROJECT_URL=${CI_PROJECT_URL} \
		--build-arg CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} \
		--build-arg GO_SOURCE=${GO_SOURCE} \
		--build-arg GO_TAG=${GO_TAG} \
		--build-arg PROJECT_TAG=${PROJECT_TAG} \
		--build-arg FROM_PREFIX=${FROM_PREFIX} \
		--build-arg SPECIFIC_IMAGE=${SPECIFIC_IMAGE} \
		--build-arg VARIANT=${VARIANT} \
		.
	@docker images ${CI_REGISTRY_IMAGE}/${PROJECT_TAG}/${SPECIFIC_IMAGE}:${GO_TAG}

.PHONY: .docker.devcontainer
.docker.devcontainer:
	@SPECIFIC_IMAGE=devcontainer $(MAKE) .docker
	docker run --rm ${CI_REGISTRY_IMAGE}/${PROJECT_TAG}/devcontainer:${GO_TAG} \
		cat /devcontainer-lib/template/.devcontainer/Dockerfile

.PHONY: .docker.gitlab-ci
.docker.gitlab-ci:
	@SPECIFIC_IMAGE=gitlab-ci $(MAKE) .docker

.PHONY: alpine.docker.devcontainer
alpine.docker.devcontainer:
	@GO_TAG=${GO_ALPINE_TAG} \
		VARIANT=alpine \
		$(MAKE) .docker.devcontainer

.PHONY: alpine.docker.gitlab-ci
alpine.docker.gitlab-ci:
	@GO_TAG=${GO_ALPINE_TAG} \
		VARIANT=alpine \
		$(MAKE) .docker.gitlab-ci

.PHONY: debian.docker.devcontainer
debian.docker.devcontainer:
	@GO_TAG=${GO_DEBIAN_TAG} \
		$(MAKE) .docker.devcontainer

.PHONY: debian.docker.gitlab-ci
debian.docker.gitlab-ci:
	@GO_TAG=${GO_DEBIAN_TAG} \
		$(MAKE) .docker.gitlab-ci

.PHONY: .template
.template:
	@CI_PROJECT_URL=${CI_PROJECT_URL} \
	CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} \
	GO_TAG=${GO_TAG} \
	PROJECT_TAG=${PROJECT_TAG} \
	SPECIFIC_IMAGE=devcontainer \
	TEMPLATE_VARIATION=${TEMPLATE_VARIATION} \
		scripts/template-bundler.sh \
		user-template/ \
		${PACKAGE_DIR}/${OUTPUT_FILE}

.PHONY: alpine.template.apply
alpine.template.apply:
	@GO_TAG=${GO_ALPINE_TAG} \
	TEMPLATE_VARIATION=alpine \
		$(MAKE) .template

.PHONY: debian.template.apply
debian.template.apply:
	@GO_TAG=${GO_DEBIAN_TAG} \
	TEMPLATE_VARIATION=debian \
		$(MAKE) .template

.PHONY: all.template.packages
all.template.packages: alpine.template.package debian.template.package

.PHONY: alpine.template.package
alpine.template.package:
	@mkdir -p ${PACKAGE_DIR}
	@OUTPUT_FILE=go_kit_alpine_${PROJECT_TAG} \
		$(MAKE) alpine.template.apply

.PHONY: debian.template.package
debian.template.package:
	@mkdir -p ${PACKAGE_DIR}
	@OUTPUT_FILE=go_kit_debian_${PROJECT_TAG} \
		$(MAKE) debian.template.apply


.PHONY: test.setup.alpine
test.setup.alpine: test.cleanup
	@$(MAKE) alpine.docker.devcontainer test.template.package.usage

.PHONY: test.template.restore
test.template.restore:
	git restore user-template/

.PHONY: test.cleanup
test.cleanup:
	@rm -rf ${PACKAGE_DIR}

.PHONY: test.template.package.usage
test.template.package.usage: test.cleanup
	@mkdir -p ${PACKAGE_DIR}/test-alpine ${PACKAGE_DIR}/test-debian
	@$(MAKE) alpine.template.package debian.template.package
	@tar -C ${PACKAGE_DIR}/test-alpine -zxf ${PACKAGE_DIR}/go_kit_alpine_${PROJECT_TAG}.tar.gz 1>/dev/null
	@sh -c "cd ${PACKAGE_DIR}/test-debian && unzip ../go_kit_debian_${PROJECT_TAG}.zip" 1>/dev/null
	@sh -c "cd ${PACKAGE_DIR}/test-alpine && git init && git add . && git status && git commit -m 'Initial commit'" 1>/dev/null
	@sh -c "cd ${PACKAGE_DIR}/test-debian && git init && git add . && git status && git commit -m 'Initial commit'" 1>/dev/null
	@echo "Tests ready at ${PACKAGE_DIR}/test-{alpine,debian}"

# For CI usage

MK_CI_IMAGE ?= ${CI_REGISTRY_IMAGE}/${PROJECT_TAG}/${SPECIFIC_IMAGE}:${GO_TAG}
MK_FROM_PREFIX ?= ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/

.PHONY: .ci.docker.build
.ci.docker.build: .ci.docker.pull .docker

.PHONY: .ci.docker.pull
.ci.docker.build.pull:
	docker pull ${CI_IMAGE} 2>/dev/null || true
	docker pull ${CI_REGISTRY_IMAGE}/${PROJECT_TAG}/main:${GO_TAG} 2>/dev/null || true

.PHONY: .ci.docker.push
.ci.docker.push:
	docker push ${CI_IMAGE} || true

.PHONY: ci.docker.alpine
ci.docker.alpine: SPECIFIC_IMAGE = devcontainer
ci.docker.alpine: CI_IMAGE = ${CI_REGISTRY_IMAGE}/${PROJECT_TAG}/${SPECIFIC_IMAGE}:${GO_ALPINE_TAG}
ci.docker.alpine: GO_TAG = ${GO_ALPINE_TAG}
ci.docker.alpine: .ci.docker.build .ci.docker.push
	@echo "The following docker image was built and pushed:"
	@echo "\e[44m${CI_IMAGE}\e[0m"

.PHONY: ci.template.packages
ci.template.packages:
	@mkdir -p ${PACKAGE_DIR}
	@GO_TAG=${GO_ALPINE_TAG} OUTPUT_FILE=${TEMPLATE_ALPINE_PKG} $(MAKE) alpine.template.apply
	@GO_TAG=${GO_DEBIAN_TAG} OUTPUT_FILE=${TEMPLATE_DEBIAN_PKG} $(MAKE) debian.template.apply
