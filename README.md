# Go templates

Go templates for CI Docker image, GitLab-CI image, and GitLab-CI lib

## How to use

- Open the latest [gitlab release](https://gitlab.com/geekstuff.dev/templates/go/basic/-/releases)
- Follow the instructions
- Open folder with VSCode
    - Install the recommended [remote container extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)
    - A `Reopen in container` popup should appear
    - Otherwise F1, search and run `Reopen in Container`
